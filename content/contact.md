+++
title = "Contact"
date = "2016-02-17"
sidemenu = "true"
description = "How to contact us"
+++

contact at chezzles dot com

7813 055 055

+91 80 65 000 229

+91 99 000 25739

For Directions, [click here] (https://www.google.com/maps/place/Zugzwang+Chess+School/@12.920659,77.6649533,17z/data=!3m1!4b1!4m2!3m1!1s0x0:0xcda11045b0f197d2)
<img src="https://i.imgsafe.org/a236b5c.jpg" alt="ZugZwang Office" width="600">
