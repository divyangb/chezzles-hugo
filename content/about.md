+++
title = "About"
+++

The ZugZwang Way is the facilitation of the right environment for learning to thrive.
Where students are mentored, not lectured.
Where practical experimentation takes precedence over memorization of facts.
Where intrinsic motivation reigns supreme.

The "ZugZwang Way" is a way of learning. A way of life.

ZugZwang Academy was founded for the right reasons - passion for mentoring children & intellectual development through learning.
It was with the beleif that the present educational model is flawed. Enrichment Education is the way of the future.
This is our 5th year of an uphill battle to change the way children learn and become smarter. In this time, we've mentored
over 1250 students. 

ZugZwang Academy was founded by Bharath Divyang, an IIT-IIM alumnus.

Founder's Message to Parents

> "If you desire to invest in your child’s mental & intellectual growth, there is no better tool to facilitate that development than scholastic chess. I have seen chess transform me from an average student to crème de la crème in the nation – all within a span of 4 years. Whether you dream of your child entering academics, creative arts, sports, business or professional service, give your child this gift of life-long learning & development. You’ll be surprised how far flung and long lasting the impact can be”
