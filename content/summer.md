+++
date = "2016-06-01"
title = "Summer Program"
comment = "false"
description = "Year 2016"
+++

# ZugZwang Summer Program

If you'd attended our 2016 summer program, we hope you had a hoot of a time learning great mind enriching stuff.

In case you were one of the few to miss out on 2016, do not fret. 

Remember time flies and summer of 2017 will be around the corner in a few months. Be sure to check with us. 


But while waiting for the summer, how about considering our offerings for the regular academic year?
We do have something for anyone interested in pursuing an enriched education to prepare their kids for the 21st century.
