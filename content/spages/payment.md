+++
date = "2016-03-09"
title = "ZugZwang Chezzles Payment"
sidemenu = "false"
description = "Payment Options"
+++

### Pay Online ###

If you know the amount to be paid, [Pay Online] (https://www.instamojo.com/@chezzles)
(Usually after the invoice / estimate has been raised)

If you'd like to purcase through our digital store, [Visit Store] (https://www.instamojo.com/chezzles/?ref=profile_bar)

### NEFT & Bank Transfer ###

<table class="pure-table pure-table-striped pure-table-horizontal">
    <tbody>
        <tr>
            <td> Bank </td>
            <td> HDFC Bank </td>
        </tr>
        
        <tr>
            <td> Branch </td>
            <td> Koramangala II </td>
        </tr>
        
        <tr>
            <td> Address </td>
            <td> Intermediate Ring Road, Ejipura </td>
        </tr>
        
        <tr>
            <td> City </td>
            <td> Bangalore </td>
        </tr>
        
        <tr>
            <td> Account Name </td>
            <td> ZugZwang Chess School Pvt. Ltd. </td>
        </tr>
        
        <tr>
            <td> Account Number </td>
            <td> 17582560000438 </td>
        </tr>

        <tr>
            <td> IFSC Code </td>
            <td> HDFC0001758 </td>
        </tr>
    </tbody>
</table>

### Cheque & Draft ###

Please make the cheque in the name of "ZugZwang Chess School" or "ZugZwang Chess School Pvt. Ltd.". 
Cheques with errors and countersigns will not be accepted.

### Credit & Debit Card ###

Available at the [Bellandur COE] (https://www.google.com/maps/place/Zugzwang+Chess+School/@12.920659,77.6649533,17z/data=!3m1!4b1!4m2!3m1!1s0x0:0xcda11045b0f197d2)
where you can physically swipe your credit / debit cards.

### Paypal ###

This option is available only for customers outside India. 
Those within India are requested to use other forms of payments. 
Please make payment after the invoice has been generated.

info@zugzwang.in

### Cash ###

Cash Payments can be made at our Bellandur CoE. If you do not receive a receipt within 12 hours, please 
[Email Us] (mailto:info@zugzwang.in) immediately. We request you to refrain from using this mode as much as possible.