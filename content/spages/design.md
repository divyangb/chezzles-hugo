+++
date = "2016-06-01"
title = "Program Design"
comments = "false"
+++

All our programs are organized into three phases of learning
1. enjoy
2. improve
3. excel

The beginner phase ("enjoy") will instill the joy of learning and build motivation, without which all education fails.

During the second phase ("improve"), the threshold has been crossed and the learning can be more result oriented.

During the final phase ("excel"), the metrics to measure increase and the expections are reset to be more realistic.