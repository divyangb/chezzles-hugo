+++
date = "2016-06-01"
title = "Regular Programs"
comment = "false"
description = "Year 2016"
+++

"What we measure, we improve"
and eventually, we excel.

Analytics is going to be a key part of the program for this academic year.

__The key points to remember__

* We are shifting into a [membership] (../regular/membership) model for chess (Rank 02 & above)
* All enrolled students will be offered Chesskid Gold Membership (unlimited puzzles) as part of the membership package
* ZugZwang Online (an online course portal) containing courses, video lessons and test series will be integrated as a part of the membership
* PTMs will be done formally to accelerate the learnings and fix any possible problems early on.
* The higher level chess classes (Rank 02 & above) will be of two hour duration
* Quality of learning, analytics and awareness of strengths and weaknesses will be key areas of focus.

__Next Steps__

Please fill out the [registration form] (https://docs.google.com/a/chezzles.com/forms/d/1FZyG6fmLVeVd0zdYMBOwVRWvCps42qgVtYktSQGy0K0/viewform)

The schedule was created based on the inputs received from many of you. We've tried our best to suit the schedule.

Please indicate in case the schedule doesn't work for you.

After the registration form has been filled out, you'll receive a call in case you need help with scheduling or fee details.

If all is in place, we shall generate the invoice along with the instructions for the payment.