---
author: bharath divyang
comments: true
date: 2016-03-09
layout: post
slug: zugzwang-summer-program-2016
title: ZugZwang Summer Program
categories:
- summer program
tags:
- summer program
---

Things are getting a little hectic and the team is really pushing hard to get the summer program design.
We are almost there.

Innovation is our mantra and really happy to introduce a few new courses this summer.

For the grand details about the summer program, please check out [zugzwang summer program] (../summer)

Hope our students enjoy what we have to offer this summer.
Looking for a fun-filled learning experience.