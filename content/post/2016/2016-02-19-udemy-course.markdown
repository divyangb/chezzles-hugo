---
author: praveen sagar
comments: true
date: 2016-02-19 08:42:00+05:30
layout: post
slug: udemy-course
title: Chess 101 on Udemy
categories:
- online coures
- chess 101
- udemy
---

Absolutely thrilled to see the Chess 101 course finally live on Udemy.

Check out the course - [Chess 101] (https://www.udemy.com/chess-101-everything-you-need-to-know-about-the-basics/)

Praveen Sagar has been absolutely fantastic in explaining some of the basic concepts of chess.
It is the best starting point for your child to begin chess.