---
author: bharath divyang
comments: true
date: 2016-05-20
layout: post
slug: service-tax-change
title: Change in Service Tax
categories:
- service tax
tags:
- tax
---

Krishi Kalyan Cess (KKC) of 0.5 % will be added to the existing service tax rate starting June 1st, 2016, making the effective Service Tax rate to 15 %.

All invoices generated after June 1st will include the new service tax rate in addition to the program fee.

ZugZwang Academy is registered legal entity and it is our fiduciary duty to collect service tax on behalf of the Government of India and file returns periodically, we we've been doing for the past few years.

> "There are only two certainties in life - death & taxes"

To learn more about India's tax regime, please check [Taxation in India] (https://www.bankbazaar.com/tax.html)