---
author: bharath divyang
comments: true
date: 2016-05-15
layout: post
slug: enrichment-education
title: Enrichment Education
categories:
- philosophy
tags:
- enrichment education, philosophy
---

"What we measure, we improve"
and eventually, we excel.

but what happens when we measure a tad too much ?

The problem with today's education is that the system has turned students to be __too__ result-oriented. 
Today's education prepares a student for standardized tests and in the process the joy of learning is lost.

In the long run, the grade point averages and marks become mere statistic. 
In the long run, what matters is the real crystallized wisdom of learning.
In the long run, only fluid intelligence can help a child through life's myriad challenges.

Since inception, ZugZwang Academy has prepared students to build their fluid intelligence skills.

Fluid intelligence is the amalgamation of the critical thinking, logical reasoning & problem solving skills.
Our vision has been to help children enrich their education by focusing on the 21st century life skills.

Ultimately, one has to learn to strike the balance between need to measure the effort and ability to enjoy the effort.