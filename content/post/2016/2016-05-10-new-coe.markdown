---
author: bharath divyang
comments: true
date: 2016-05-10
layout: post
slug: new-coe
title: New Center of Excellence
categories:
- centers
tags:
- centers
---

Absolutely thrilled about the second center of excellence starting today at Kalyan Nagar, Bangalore.

[Praveen Sagar] (https://ratings.fide.com/card.phtml?event=146242131) will be heading the center.

While we have operated out of multiple locations (32 to be precise) in Bangalore in the past, they have been 'beta' centers - mostly apartments & playschools.

Bellandur CoE has been the only one thus far and it is only in our fifth year that we're opening the second one. The primary reason was to perfect our courses, process & models
before looking at newer geographices. But now the time has arrived ...

Here's wishing Praveen & his team the best.

The center will be commence operations from mid-June and will offer the following courses

* Scholastic Chess
* Robotix
* Scratch Coding
* Wonder Math & COMP (Math Enrichment Course)
* Wonder Words & Spell Bee