---
author: bharath divyang
comments: true
date: 2016-02-21 09:15:00+05:30
layout: post
slug: life-coaching
title: Career & Life Coaching
categories:
- online coures
- chess 101
- udemy
tags:
- life coaching
- career coaching
- mentorship
---

Innovation has been the keystone to our progress & growth at ZugZwang.
Today, we take another step by starting our career & life coaching program.

Philosophy of Life Coaching

Life rests on three pillars - knowledge, skills & motivation.
The three need to be in balance and at healthy expected levels.
In this program, our mission is to help the person find this balance.
And to coach & mentor them through the process of balancing the growth of all three pillars.

Long Term

Clarity of Goals
Alignment of Goals & Interest
Identify natural Intellectual bent of mind
Help build a Career Plan & Strategy

Short Term

Coaching on facing challenges like exams & competition
Mentoring on academic and work challenges
Helping with planning & execution of short term goals
Identifying ways to maximize potential for short-term projects
Working on building effectiveness in life (time management, priorities et al)