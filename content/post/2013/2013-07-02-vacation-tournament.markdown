---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-02 06:34:00+00:00
layout: post
slug: vacation-tournament
title: Vacation & Tournament
wordpress_id: 6517
categories:
- News
---

It is not everyday you see Kashmir & Chess in the same sentence.   
  
If you have wanted to visit Srinagar - this might be the best opportunity. J & K Chess federation is conducting a FIDE rated tournament (under 2000) from Oct 2 - 6. For more details check out the [ Article ](http://news.aicf.in/2013/06/kashmir-calling-chess-players/)  

