---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-25 07:51:00+00:00
layout: post
slug: agile-methodology-for-chezzles
title: Agile Methodology for Chezzles
wordpress_id: 6511
categories:
- chezzles website
- programming
---

I programmed for 6 years while I was in the technology industry and never really understood it the way I'm currently able to comprehend its nuances as I work on the chezzles website. There were so many times I've heard about Agile methodologies for programming, but never really truly understood its potential.  
  
But today as I develop the chezzles website from scratch I'm able to see it manifesting in a way beyond imagination.  
  
I'll let the result speak for itself.  
In a few more days.
