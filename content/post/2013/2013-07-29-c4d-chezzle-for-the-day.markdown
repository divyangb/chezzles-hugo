---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-29 05:32:00+00:00
layout: post
slug: c4d-chezzle-for-the-day
title: C4D - Chezzle for the Day
wordpress_id: 6510
categories:
- Chess
- News
---

<img src="https://i.imgsafe.org/39e1f58.jpg" alt="Reti'na Display" width="400">

White to move & draw

Considered by many as the most famous chess puzzle. A master piece by Richard Reti.  
  
Check out our facebook page - [Chezzles](https://www.facebook.com/ranksnfiles) - for the solution in another week's time.  


  

