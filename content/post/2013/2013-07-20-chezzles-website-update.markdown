---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-20 05:46:00+00:00
layout: post
slug: chezzles-website-update
title: Chezzles Website - Update
wordpress_id: 6515
categories:
- News
---

I had once read about a pixar's guide to writing animated movies. And I distinctly remember step # 18. It read:  
  
18. Rewrite the entire story now.  
  
My dalliance with web application development for chezzles seems to be taking a page out of pixar's book. After I figured out how to host chezzles, I learn there is a whole lot of things that can be done and that it should be done for excellent customer experience.  
  
I wouldn't settle for anything less.  
  
The wait has to extend as I chug through copious lines of code.  
  
The destination, though, is not far.
