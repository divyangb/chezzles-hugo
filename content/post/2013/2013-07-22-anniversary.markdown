---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-22 17:55:00+00:00
layout: post
slug: anniversary
title: Anniversary
wordpress_id: 6513
categories:
- News
---

In another 6 days, we complete 1 year of conducting chess classes in Bangalore. My first 3 students who attended the first class are still enrolled. I will always be indebted to you for trusting in me - Rennie Patric, Srikrishna G & Harini Anand (& respective parents). It has been quite the journey and we've grown from strength to strength. But it all happened due to that first class. That first step.
