---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-06-26 05:53:00+00:00
layout: post
slug: grit-the-mark-of-a-tireless-mind
title: Grit - The mark of a tireless mind.
wordpress_id: 6518
categories:
- News
---

The more I teach chess, I've come to realize that even the most gifted of minds amounts to dust without effort, hard work, smart work, perseverance and grit. Behind every single achievement is a tireless mind.   
  
Grit, I believe, is the single most important factor in differentiating success from failure. This is true in life as much as it is in chess. Instill that in your child. They're smarter than you think and you know that. But they're not as strong as they need to be. 
