---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-21 07:43:00+00:00
layout: post
slug: zugzwang-chess-programming
title: ZugZwang Chess & Programming
wordpress_id: 6514
categories:
- algorithms
- scientific learning
- structured thinking
---

Why ZugZwang students are going to be amazing programmers.  
  
I think there are two ways of learning chess. One, through traditional osmosis. You spend enough time with a board and someone who plays and you'll going to be pick things up. This is how I became a chess player.  
  
Second, and the method I emphasize at ZugZwang, is to learn chess in a methodical and structured fashion. And what I've come to realize is that this is so akin to thinking about algorithms and heuristics that I have a strong belief that most ZugZwang students are going to be very adept when it comes to programming.  
  
While most pursuits in life needs one to be structured and organized. Programming is one that is unforgiving of those who aren't.
