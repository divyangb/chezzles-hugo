---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-20 05:06:00+00:00
layout: post
slug: emergency-sickness
title: Emergency & Sickness
wordpress_id: 6516
categories:
- News
---

I'm hoping to begin using the blog as a mouthpiece to convey updates, news & sundry.  
This week has been particularly bad for Team ZugZwang, which is plagued by emergencies and hospital visits. The weather hasn't been friendly either in changing the mood & spirit.  
  
So please bear with us as we shall rise like phoenixes !  
  
~ BD
