---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-12-01 01:47:00+00:00
layout: post
slug: how-to-obtain-a-fide-rating
title: How to obtain a FIDE Rating
wordpress_id: 6509
categories:
- Chess
- News
---

In Chess, you always deal with black & white - no ambiguity and no grey areas. While the procedures and rules of FIDE might be an obscure shade of grey at times, the process of obtaining a rating or any title for that matter is anything but.  
  
If you want a rating, you HAVE to beat a rated player.  
If you want to become an IM, you HAVE to beat an IM  
If you want to become a GM, you HAVE to beat another GM  
And as Magnus just did - you want to become a World Champion, you have to beat the current World Champion.  
  
There are no short cuts and there is no beating the system.  
  
So here goes the means to achieve the first step in that long journey - a rating  
  
This is the excerpt from the current FIDE guideline  
  
For an unrated player’s first performance to count, he must play at least 3 games against rated opponents; score at least 1 point; and the rating based on the tournament result at its conclusion must be at least 1000.  
  
If based on results obtained under 6.4, a minimum of 9 games played against rated opponents.  
  
The condition of a minimum of 9 games need not be met in one tournament. Results from other tournaments played within consecutive rating periods totalling not more than 26 months, are pooled to obtain the initial rating.  
  
The rating is at least 1000  
  
So basically, you have to  
  
Open the account by  
  


  1. Playing at least 3 games against rated opponents in one tournament
  2. Score 1 point (1 win or 2 draws) out of those 3 (min) games.
  3. Rating in that tournament is above 1000 (which is typically the case, unless you were to lose all your unrated games)
  
Complete the Formalities  
  


  1. By playing 9 rated opponents over a period of 26 months across tournaments, once you've opened the account
  2. Ensure your final rating is above 1000

How the rating is a calculated is a topic for another day.

Hope this helps clear up things. Feedback welcome.

  
  
  

