---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-07-23 15:46:00+00:00
layout: post
slug: 5-new-5-more-to-go
title: 5 new & 5 more to go
wordpress_id: 6512
categories:
- News
---

Five more venues schedule to open in August.  
1. Akme Ballet (next to Marathalli Total Mall)  
2. Nagarjuna Maple Heights (On ORR near KR Puram Junction)  
3. Ramamurthy Nagar (Address to be decided)  
4. Kasavanahalli (Owner's Court)  
5. Harlur Road (near Kudlu)  
  
If you're from any of these places, please contact us (info@zugzwang.in) for details.  
  
I'm also looking for to start in the following areas and if you can help me with a right venue, I'd be forever grateful.  
  
1. Indiranagar / Ulsoor / CMH Road  
2. Sahakar Nagar / Hebbal  
3. JP Nagar  
4. Jayanagar  
5. Electronic City
