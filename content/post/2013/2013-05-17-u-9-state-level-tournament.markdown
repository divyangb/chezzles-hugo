---
author: bharath.divyang@zugzwang.in
comments: true
date: 2013-05-17 18:59:00+00:00
layout: post
slug: u-9-state-level-tournament
title: U-9 State Level Tournament
wordpress_id: 6519
categories:
- News
---

103 Boys and 24 Girls battled it out in the U-9 category at the State Youth Center in Nrupthunga Road, Bangalore. The first tournament where a significant number represent ZugZwang Chess School. I hope this experience helps the kids grow in maturity and motivation.  
  
On another note, I feel the numbers are way too low and needs to be at least twice as much in two more years. Hope Karnataka sees a surge in interest and has many more participants in the years to come. Jai Karnataka !!  
  
The journey is definitely the proverbial thousand miles. But the first few baby steps have been taken.
