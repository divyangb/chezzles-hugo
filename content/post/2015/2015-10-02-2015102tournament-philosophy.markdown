---
author: bharath.divyang@zugzwang.in
comments: true
date: 2015-10-02 07:13:07+00:00
layout: post
slug: 2015102tournament-philosophy
title: Tournament Philosophy
wordpress_id: 32
categories:
- Chess
- News
---

"A computer once beat me at chess, but it was no match for me at kick boxing." ~Emo Philips.

We have revolutionized Chess learning. Now we're set to do the same to Chess tournaments. We call it a "match" for a reason. Expecting Mary Kom to beat Mike Tyson in a duel is almost comical. Yet, that is exactly what happens in most Chess tournaments. If you're a good player in the tournament (remember, its all relative), you can be rest assured to win 80% of your games and be challenged only for remaining 20%. If you're weak player for the tournament, the opposite would be true. While many of you attend tournament for the experience - Is this the experience you really want ? To end it all, less than 10% of the kids go home as winners. If a tournament offers more prizes, it is usually conciliatory.

Instead imagine this - every participant belongs to a category, carefully chosen based on experience, age, playing strength and rating. Every round is exciting and not predetermined. There is a podium finish (gold, silver & bronze) in each category. Now, that's disruption. Unlike most tournaments, this will not be a Swiss System (which makes the odds of winning very slim). Instead, we will be doing a Single Round Robin by dividing students according to their playing strength giving them a fair shot at winning in their category. Participants will be divided into categories based on playing strength. Each category will consist of 6 or 4 students. Every single board WILL have a clock. Hence the number of participants is limited to 48 for the tournament. So Register early. There will be a total of 5 rounds with each being a 20 minute game. Details will be shared before the tournament. The first round will begin at 4:15 PM and your clock will start implying you'll have lesser time to begin if you are late.

 
