---
author: bharath.divyang@zugzwang.in
comments: true
date: 2015-01-07 06:38:00+00:00
layout: post
slug: opening-traps
title: Opening Traps
wordpress_id: 6505
categories:
- Chess
- News
---

One of the sure shot ways of losing a game is to fall into an opening trap. The game is over even before its begun. The fool's mate, scholar's mate & nincompoop's mate are all really early Rank One opening traps in a way. However, I came across a collection of some really uncommon opening traps and it would be nice for you to check them out and be prepared not to fall for them.  
  
Click on the link below:  
  
[10 Deadly Opening Traps](http://www.thechessworld.com/learn-chess/1-openings/376-openings-traps)  
  
I loved trap #7 - best example of bad development that leads to helpless positions.  
  

