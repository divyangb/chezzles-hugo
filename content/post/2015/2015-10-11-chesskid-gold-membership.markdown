---
author: bharath.divyang@zugzwang.in
comments: true
date: 2015-10-11 02:37:44+00:00
layout: post
slug: chesskid-gold-membership
title: Chesskid.com Gold Membership
wordpress_id: 34
categories:
- Chess
- News
---

We have partnered with Chesskid.com to bring the best of Chess learning to you for a discounted price. We have been using Chesskid.com extensively to motivate children to play puzzles, discover opening through video lessons and play & monitor games regularly. We have also used to the game & puzzle ratings to accelerate the learning and interest.

Over the last one year, we have noticed an extended access to puzzles, lessons & games has helped the Gold Membership students to accelerate faster than the rest. Furthermore, the interest levels have sustained for longer. Hence, I strongly urge you to take advantage for the offer and make the best use to complement the learning at Chezzles.

We offer the Annual Gold Membership at a 37% (after tax) discount specially for our students at Chezzles. [Purchase standalone, the membership costs $49.99 or Rs. 3250]. The special prize for Chezzles students alone is Rs. 1800 + service tax.

Once your payment is received, we will upgrade your account to Gold within 2 working days. In case your account hasn't been created, we will create an upgraded account and share the details with you.

Wishing you all the best.


## [Click here to Enroll](https://www.instamojo.com/chezzles/chesskidcom-gold-membership/)
