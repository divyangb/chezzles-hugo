---
author: bharath.divyang@zugzwang.in
comments: true
date: 2015-03-29 04:16:00+00:00
layout: post
slug: 2015329i-know-what-you-should-do-this-summer
title: I know what you should do this summer ....
wordpress_id: 6565
categories:
- Chess
- News
tags:
- resources
- summer
---

As the academic year comes to an end and you are looking at 2 months of summer and surf, it would be important to know what you can do keep your brain in top shape. It is a researched fact that students who do not engage in any mental activity during a long break tend to lose out when they get back in the grind after the summer. Remember, learning is continuous and it can be fun.

**Chess**

Set up a weekly regimen to do the following

  1. Play Games
  2. Solve Puzzles
  3. Watch Videos
  4. Read Books

It would help you with two things - keep things balanced and not take you to the extremes of 'famine or feast' manner of doing things. Please fill out form that will help keep track of your effort.

[Weekly Tracker](https://docs.google.com/a/zugzwang.in/forms/d/14Ymx8ytDjv-T2dISvvZ_qaCMzqXNyziaNtk5ErLLvnE/viewform)

_Resources_

Play Games

The best way is to find a person who you can play with. Nothing beats that.

If not, here are the websites that would help you play online.

  * [chess.com](http://chess.com)
  * [chess24.com](http://chess24.com)
  * [chesskid.com](http://chesskid.com)

Puzzles

The above mentioned websites also offer daily puzzles in the form of tactics trainer.  In addition, I'd strong recommend the following app, which is available in both iOS and Android platforms

iChess

This app has close to 900 puzzles and it is one of the best packages available. Do not try to solve these puzzles in less 10 seconds and keep moving on. Set at least 5 minutes for each puzzles. Remember, these were actual positions in actual games and the players must've taken at least 5 minutes before deciding their next move.

Videos & Books

Youtube videos and chess books are rich sources of information and I'd strongly urge you to use your time to do both of these regularly. Here are some of the channels you can follow

  * [Dirty Chess Tricks](https://www.youtube.com/channel/UCji6ITCdOOmxUzd6R2TonZQ)
  * [Chess Website](https://www.youtube.com/user/thechesswebsite)
  * [Chess Network](https://www.youtube.com/user/ChessNetwork)

I also plan to start a free channel on youtube. You can follow the updates at this blog for the link and other details.

**NON CHESS**

This is a great time to pick up a few skills. You could try your hand at Rubix, Go or Programming. Or you could just watch videos at these great youtube channels

  * [Crash Course](https://www.youtube.com/user/crashcourse)
  * [Vlog Brothers](https://www.youtube.com/user/vlogbrothers)
  * [Minute Physics](https://www.youtube.com/user/minutephysics)

Finally, remember the quality matters, not the quantity. So focus on how deep the hours are not how long they are. Either way, get your neurons on fine, this summer !!

Good luck & have a great holiday.

If you have any comments, views or critiques, please leave a comment below or send me an email at contact at chezzles dot com.

 






  


  

