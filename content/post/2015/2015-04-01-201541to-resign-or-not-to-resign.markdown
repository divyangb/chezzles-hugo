---
author: bharath.divyang@zugzwang.in
comments: true
date: 2015-04-01 06:49:01+00:00
layout: post
slug: 201541to-resign-or-not-to-resign
title: To resign or not to resign ...
wordpress_id: 30
categories:
- Chess
- News
---

It is a common phenomenon among chess players - to give up; to resign. Especially among higher rated players. Higher the level of play, greater the number of resignations.

The logic behind this seemingly cowardly exit is simple - once you know the position is dead lost, why waste any energy trying to prolong the agony. Sound.

While it is true among masters, this is completely wrong among beginners and amateurs.

When you're a beginner, there are valuable lessons to be learnt at every phase of the game. Chess is all about coming up with the best move for the given situation. So even if you're losing badly, there are good moves to be made. Don't lose out the opportunity.

Once you've let the tension drop by blundering a piece or losing the plot completely, it is very tempting to give up and think of the next move - but resist that urge and become a fighter and play until the very end. Unlike many sports, there might not be a fairy tale ending waiting with the unexpected upset but there is valor and courage in fighting till the end.

DO NOT RESIGN any game until the absolutely novice in the room knows why you should resign. PLAY for the game, not for the result.





