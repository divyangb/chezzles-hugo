---
author: chezzles
comments: true
date: 2015-12-08 16:24:06+00:00
layout: post
slug: practice-makes-perfect
title: Practice Makes Perfect
wordpress_id: 6103
categories:
- Chess
---

Practice is the art of endless repetition till perfection.

However, repetition is one of the hardest things for a human mind to do.

The task becomes boring. Often loses its very meaning.

Perseverance is the only thing that matters at this stage.

That's when the person who perseveres begins to see light at the end of the tunnel.

Without practice, without the endless hours of repetition of the skill, there is no glory.

No perfection.




