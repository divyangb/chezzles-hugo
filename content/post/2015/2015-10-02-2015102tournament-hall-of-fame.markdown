---
author: bharath.divyang@zugzwang.in
comments: true
date: 2015-10-02 08:36:36+00:00
layout: post
slug: 2015102tournament-hall-of-fame
title: Tournament Hall of Fame
wordpress_id: 33
categories:
- Chess
- News
tags:
- Tournament
---

Here are some of the hall of famers for Chezzles this year.

### Bangalore Rural - Under 16

Aravindakshan - 1st Place

Akshat Sisodhia - 2nd Place

### BNCF Tournament - Group 10

Akshat Sisodhia - 4th Place

### CHAMPIONS CHESS ACADEMY TOURNAMENT- UNDER 07

Anirudh BS - 1st Place

### Champions Chess Academy Tournament- Under 10

Rishit Pathak - 5th

Shreyas Pathak - 6th

Anirudh Archak - 12th

Devansh Mishra - 20th

### Krishnarao Memorial - Under 7

Suhani Choudhary - 1st Place

### Karnataka State - Under 15

Reetish Padhi - 2nd Place

 
