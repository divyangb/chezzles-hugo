---
author: bharath.divyang@zugzwang.in
comments: true
date: 2014-11-23 06:32:00+00:00
layout: post
slug: calling-for-teach-prenuers
title: Calling for teach-prenuers !!
wordpress_id: 6507
categories:
- Chess
- News
---

Are you a teacher dealing with children day-in day-out ? Do you wonder about the impact you're creating? Are you looking to increase your impact - both financial & otherwise? I understand how hard it can be.  
  
I would like to offer my assistance in making your teaching methodologies more inspiring and effective. Sign up for a free 2-hr workshop on the 30th of November, 5:00 PM - 7:00 PM at our [ZugZwang Office](http://bit.ly/zz_map).  
  
Please fill out the form below to enroll.  
[Chezzles Teacher Training Workshop](https://docs.google.com/a/chezzles.com/forms/d/1jsqilOLZAEyZdrQDrVhepaPzsoPLBvxJ88rp-b6rwSg/viewform)  
  
_Background to the Workshop_  
In the last few years I have donned a lot of hats and hardest one to wear is that of a teacher. Shaping young minds of the next generation is no easy task. Furthermore, I obtained significant insight into the uninspired "bad" teachers too. While we have crossed 800 students as of date, we have also trained a significant number of teachers to teach the right way.  
  
I can help you become a really great teacher. All it takes is to sign up for a 2 hour workshop.  
  
I look forward to meeting you.  
  
Regards,  
Bharath Divyang  
Founder & CEO.  
  

