---
author: bharath.divyang@zugzwang.in
comments: true
date: 2014-10-31 07:16:00+00:00
layout: post
slug: wonderful-endgame-puzzle
title: Wonderful Endgame Puzzle
wordpress_id: 6508
categories:
- Chess
- News
---

<img src="https://i.imgsafe.org/a6c57f7.jpg" alt="Endgame Puzzle" width="400">
  
Here's wonderful chess puzzle to have 'em grey matter oozing.  
  
White to move and win !  
If one were to take it to its end, this position is a beautiful illustration of how one has to consider various combinations and trees during endgame study.  


  

