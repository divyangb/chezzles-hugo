+++
date = "2016-06-01"
title = "Membership Benefits"
comments = "false"
+++

__Membership entails the following__

* 36 classroom sessions (72 hours of chess for higher ranks)
* chess kid gold membership
* zugzwang online membership (online videos, course material & test series)
* student analytics (qualitative & quantitative)
* 3 formal PTM (progress & plans will be discussed)
* gamification of learning (green cards & rock star of chess competition)
* tournament advise (pre planning & post analysis)
* Books, handouts, stationery & scoresheets
* Experimental Activities

__Membership doesn't include__

* Rating Boost (1*1 classes)
* Tournament & Competition Fee
* Other courses offered

__Experimental Activities planned for this academic year__

* Online topic-based webinars
* Grandmaster Workshops (we're planning to invite GMs to meet & greet students)
* Access to learning material (expensive books & training material)
