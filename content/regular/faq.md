+++
date = "2016-06-01"
title = "Frequenty Asked Questions"
comment = "false"
description = "FAQs"
+++

__Program Stucture & Learning Model__

_Why move to the two hour format for chess?_

_Why are you moving to a membership model?_

_What does the membership include?_

__Fee & Payments__

_What are my payment options?_

_What if I want to withdraw from ZugZwang Academy?_

_Is the fee refundable? Can it be adjusted against a future course?_