+++
date = "2016-06-01"
title = "Programs"
+++

The programs offered during this academic year (2016 - 17) are the following:

* Scholastic Chess
* Math & Game Programming
* Robotix
* Scratch Coding
* Rubix
* Wonder Math & COMP
* Wonder Words & Spell Bee