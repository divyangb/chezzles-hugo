+++
date = "2016-03-10"
title = "Scholastic Chess"
sidemenu = "false"
description = "Young Master Program"
+++

Give your child the opportunity to build strong fundamentals in Chess this summer. 

Scholastic Chess is the flagship program of ZugZwang, focusing on helping 
a child learn chess through developing better thinking skills. We focus on 
engendering critical thinking, logical reasoning and problem solving skills.

Our experienced coaches have created a brand new framework across all Ranks and 
a new structured plan to ensure that your child gets access to the 
best learning material. Over 1000+ Puzzles, Hours of Gameplay, and 
Professional Coaching from our experienced mentors available during this summer!

If you’re a new entrant, your Rank will be determined based on our assessment. 

If you're a current student enrolled in Rank 01 or Rank 02, you will be eligible
to continue your next term.

If you've just completed Rank 02 or above, we strongly urge you to consider
[Chess League] (../chessleague/)

Minimum Age - The child should've completed 5 years by Apr 1st, 2016.