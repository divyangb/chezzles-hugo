+++
date = "2016-03-08"
title = "Javabots Robotix"
sidemenu = "false"
description = "Robotics Program"
+++

In a world that's moving towards self driven cars, IoT & drones, knowledge and
prowess in the field of robotics can be quite the difference in the future. 

Javabots Robotix aims to help a child learn problem solving & reasoning skills
using a robotics engineering context. It contains a sequence of projects 
organized around key robotics and programming concepts.

The curriculum for course is developed in conjunction with the FLL [JAVAbots] (https://www.facebook.com/javabots1/?fref=ts)
team coach, Gopi Prashanth, who helped the team win the first place in the Washington
State Robotics challenge

Each project provides students with:

* An introduction to a real-world robot and the context in which it operates
* A challenge or a problem that the robot faces
* A LEGO-scale version of the problem for students to solve with their robots
* Semi-open-ended Mini-Challenges which ask students to use the skill they 
have just learned to solve a relevant small portion of the final challenge
* The Unit Challenge based on the original robot’s problem, for students to solve 
in teams as an exercise and demonstration of their mastery of the concept
* Learning opportunity to control & manipulate the robot, the various sensors (gyro, color et al)
to solve specific problems & challenges.

The course utilizes Lego EV3 robot and Mindstorms EV3 programming language.

Available for students from Grade IV & above
The kits will be provided by ZugZwang. 