+++
date = "2016-03-08"
title = "Scratch Coding"
sidemenu = "false"
description = "Algorithmic Thinking Program"
+++

Coding consists of two components - Algorithms (thinking) & Syntax (execution).
Due to the complicated nature of program syntax, most children are exposed to
coding only at a higher grade. But isn't that throwing the baby with the
bathwater? What if there were a way to help a child learn the Algorithmic Thinking
process without having to deal with messy syntax ?

The answer is Scratch Coding - a program that helps children learn the basics
of coding and algorithmic thinking through visual programming means such as
[MIT Scratch] (https://scratch.mit.edu/) and [Pencil Code] (https://pencilcode.net/).

In this course, a student will learn

1. Basics of Programming
2. Learn application through games & apps
3. Use Scratch to create simple projects & games.

Available only for students from Grade IV & above.

