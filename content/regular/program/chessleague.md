+++
date = "2016-03-10"
title = "Chess League"
sidemenu = "false"
description = "Chess GamePlay Program"
+++

Available only for current students

A significant part of learning chess is practice. We call it GamePlay.
This summer, we are offering 28 hours of guided GamePlay sessions (2 hr / session).
An intense program that will focus on

* Building the habit of game analysis
* Creating a more self aware approach to GamePlay
* Developing & storing the games in PGN
* Analysis of select games by coaches
* Tournaments - Olympiads, Standard Games (1 hr+) & Leagues
* Tournament etiquette & preparation
* Professional approach to Chess




