+++
date = "2016-03-08"
title = "Game Programming"
sidemenu = "false"
description = "Game Theory & Coding Program"
+++

The single most important skill of the 21st century is going to be coding. 
As technology makes inroads into all avenues of life, coding is no longer 
the forte of only the specialized programmer or hacker. 

This course is targeted towards high school students initiating them into 
the programmatic & algorithmic thinking to solve problems through code.

The goals of these courses are

* Introduction to programmatic & algorithmic thinking
* Basic problem solving patterns
* Flowcharting
* Familiarity with coding environments.
* Game Theory
* Game Programming
* Breaking down a Game
* Building a Visual Game (through Apps)
* Designing a Game

Students will work in teams to design and build a game during the course.

Available for students from Grade 8 & above
