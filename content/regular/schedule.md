+++
date = "2016-06-01"
title = "Regular Schedule - AY 2016 - 17"
+++

__Weekday Classes__

The weekday classes will be held between Monday to Friday between 4:00 PM and 6:00 PM.

The enrolment is usually lesser so you get to choose the slot & the program. This will change accordingly the enrolment we receive.

The biggest advantage of weekdays will be a favorable teacher-student ratio.

These sessions would be ideally suited for English & Math classes.

* * * 

__Weekend Classes__

Given below is the Friday evening schedule for Friday's Chess classes.

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Timing </th>
            <th> Class 01 </th>
            <th> Class 02 </th>
        </tr>
    </thead>
    
    <tbody>
        <tr>
            <td> 16:30 - 17:30 </td>
            <td> Chess 101 </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 17:30 - 18:30 </td>
            <td> Chess 101 </td>
            <td> Chess 102 </td>
        </tr>
        
        <tr>
            <td> 16:30 - 18:30 </td>
            <td> Rank 02 & Rank 03 </td>
            <td> Rank 04 & Rank 05 </td>
        </tr>
    </tbody>
</table>

* *  *

Listed below is the schedule for Saturday classes.

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Timing </th>
            <th> Class 01 </th>
            <th> Class 02 </th>
            <th> Class 03 </th>
        </tr>
    </thead>
    
    <tbody>
        <tr>
            <td> 10:00 - 12:00 </td>
            <td> Rank 02 & Rank 03 </td>
            <td> Rank 04 & Rank 05 </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 12:00 - 13:00 </td>
            <td> Chess 101 </td>
            <td> Chess 102 </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 13:00 - 14:00 </td>
            <td> Spell Bee & Wonder Words </td>
            <td> Rubix </td>
            <td> Scratch Coding </td>
        </tr>
        
        <tr>
            <td> 14:00 - 15:00 </td>
            <td> COMP & Wonder Math </td>
            <td> Robotix </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 15:00 - 17:00 </td>
            <td> Rank 02 & Rank 03 </td>
            <td> Rank 04 & Rank 05 </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 16:00 - 17:00 </td>
            <td> Chess 101 </td>
            <td> Chess 102 </td>
            <td> - </td>
        </tr>
    </tbody>
</table>

* *  *

Listed below is the schedule for Sunday classes.

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Timing </th>
            <th> Class 01 </th>
            <th> Class 02 </th>
            <th> Class 03 </th>
        </tr>
    </thead>
    
    <tbody>
        <tr>
            <td> 10:00 - 12:00 </td>
            <td> Rank 02 & Rank 03 </td>
            <td> Rank 04 & Rank 05 </td>
            <td> Rank 06 </td>
        </tr>
        
        <tr>
            <td> 12:00 - 13:00 </td>
            <td> Chess 101 </td>
            <td> Chess 102 </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 12:00 - 14:00 </td>
            <td> Math & Game Programming </td>
            <td> - </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 13:00 - 14:00 </td>
            <td> Scratch Coding </td>
            <td> Rubix </td>
            <td> Wonder Words & Spell Bee </td>
        </tr>
        
        <tr>
            <td> 14:00 - 15:00 </td>
            <td> Robotix </td>
            <td> COMP & Wonder Math </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 15:00 - 17:00 </td>
            <td> Rank 02 & Rank 03 </td>
            <td> Rank 04 & Rank 05 </td>
            <td> - </td>
        </tr>
        
        <tr>
            <td> 16:00 - 17:00 </td>
            <td> Chess 101 </td>
            <td> Chess 102 </td>
            <td> - </td>
        </tr>
    </tbody>
</table>