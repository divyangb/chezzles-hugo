+++
date = "2016-06-01"
title = "Fee Details"
+++

__Courses__

The following programs will be offered as courses of 13 sessions (12 classrooms + 1 PTM).

The duration of each sessions will be 1 hour. 

The frequency of classes can be once or twice per week.

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Course </th>
            <th> Fee </th>
        </tr>
    </thead>
    
    <tbody>
        <tr>
            <td> Chess 101 </td>
            <td> 4200 </td>
        </tr>
        
        <tr>
            <td> Chess 102 </td>
            <td> 4500 </td>
        </tr>
        
        <tr>
            <td> Rubix </td>
            <td> 4400 </td>
        </tr>
        
        <tr>
            <td> Scratch Coding </td>
            <td> 4700 </td>
        </tr>
        
        <tr>
            <td> Wonder Math & COMP </td>
            <td> 4000 </td>
        </tr>
        
        <tr>
            <td> Wonder Words & Spell Bee </td>
            <td> 4000 </td>
        </tr>
        
        <tr>
            <td> Robotix </td>
            <td> 5400 </td>
        </tr>
        
    </tbody>
</table>

* * * 

__Membership__

The higher level of chess will be offered as a 2 hour session in a [membership] (../static/membership) model.

The duration of each session will be 2 hours.

The frequency of classes should be limited 1 per week, except make up sessions.

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Course </th>
            <th> Silver </th>
            <th> Gold </th>
            <th> Platinum </th>
        </tr>
    </thead>
    
    <tbody>
        <tr>
            <td> Type </td>
            <td> Quarterly </td>
            <td> Half Yearly </td>
            <td> Annual </td>
        </tr>
        <tr>
            <td> # Payments </td>
            <td> 3 </td>
            <td> 2 </td>
            <td> 1 </td>
        </tr>
        
        <tr>
            <td> Payment Due Dates </td>
            <td> Jun 1, Sep 1 & Nov 15 </td>
            <td> Jun 1 & Nov 1 </td>
            <td> Jun 1 </td>
        </tr>
        
        <tr>
            <td> Rank 02 </td>
            <td> 2066 </td>
            <td> 1958 </td>
            <td> 1740 </td>
        </tr>
        
        <tr>
            <td> Rank 03 </td>
            <td> 2114 </td>
            <td> 2003 </td>
            <td> 1780 </td>
        </tr>
        
        <tr>
            <td> Rank 04 </td>
            <td> 2209 </td>
            <td> 2093 </td>
            <td> 1860 </td>
        </tr>
        
        <tr>
            <td> Rank 05 & Above </td>
            <td> 2351 </td>
            <td> 2228 </td>
            <td> 1980 </td>
        </tr>
        
        <tr>
            <td> Math & Game Programming </td>
            <td> 2114 </td>
            <td> 2003 </td>
            <td> 1780 </td>
        </tr>
        
    </tbody>    
</table>

* The fee mentioned is the monthly equivalent fee.
* The amount due will be (monthly fee * 12) / (# payments)

* * * 

For instance, if you're in Rank 02 and would like the silver option.
The amount due will be (2066 * 12) / 3 = 8265
The due dates would be Jun 1st, Sep 1st & Nov 15th.

If you were in Rank 03 & would like option 2 (gold), then the amount due would be
(2003 * 12) / 2 = 12015, which would be due on 1st Jun & 1st Nov.

If you were interested in Math & Game programming and chose the platinum membership, the fee would be
(1780 * 12) = 21360 to be paid by 1st of Jun.

* * * 

* The fee & prices does not include Service Tax. The Government Mandated Service Tax of 15%  will be levied on the final fee amount due.
* Fee does not include registration & admission charges
