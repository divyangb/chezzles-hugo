+++
date = "2016-06-01"
title = "Calendar"
+++

__Academic Calendar 2016 - 17__

For each week, the Sunday's date is mentioned.

For instance, Week 01 will have weekend classes starting on Friday, Jun 03 and ending Sunday, Jun 05.

The calendar contains the Sunday's date. If your class falls on a Friday or a Saturday, you can just adjust the dates accordingly.

There are only three weekends, where the center will be closed. Those holidays are mentioned below.

Weekday calendar will be shared with those enroling for the weekday sessions.

* * * 

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Week # </th>
            <th> Term 01 </th>
            <th> Term 02 </th>
            <th> Term 03 </th>
        </tr>
    </thead>
    
    <tbody>
        <tr>
            <td> Week 01 </td>
            <td> Jun 05 </td>
            <td> Sep 04 </td>
            <td> Dec 11 </td>
        </tr>
        <tr>
            <td> Week 02 </td>
            <td> Jun 12 </td>
            <td> Sep 11 </td>
            <td> Dec 18 </td>
        </tr>
        <tr>
            <td> Week 03 </td>
            <td> Jun 19 </td>
            <td> Sep 18 </td>
            <td> Jan 08  </td>
        </tr>
        <tr>
            <td> Week 04 </td>
            <td> Jun 26 </td>
            <td> Sep 25 </td>
            <td> Jan 15 </td>
        </tr>
        <tr>
            <td> Week 05 </td>
            <td> Jul 03 </td>
            <td> Oct 02 </td>
            <td> Jan 22 </td>
        </tr>
        <tr>
            <td> Week 06 </td>
            <td> Jul 10 </td>
            <td> Oct 16 </td>
            <td> Jan 29  </td>
        </tr>
        <tr>
            <td> Week 07 </td>
            <td> Jul 17 </td>
            <td> Oct 23 </td>
            <td> Feb 05 </td>
        </tr>
        <tr>
            <td> Week 08 </td>
            <td> Jul 24 </td>
            <td> Oct 30 </td>
            <td> Feb 12 </td>
        </tr>
        <tr>
            <td> Week 09 </td>
            <td> Jul 31 </td>
            <td> Nov 06 </td>
            <td> Feb 19 </td>
        </tr>
        
        <tr>
            <td> Week 10 </td>
            <td> Aug 07 </td>
            <td> Nov 13 </td>
            <td> Feb 26 </td>
        </tr>
        <tr>
            <td> Week 11 </td>
            <td> Aug 14 </td>
            <td> Nov 20 </td>
            <td> Mar 05 </td>
        </tr>
        <tr>
            <td> Week 12 </td>
            <td> Aug 21 </td>
            <td> Nov 27 </td>
            <td> Mar 12  </td>
        </tr>
        
        <tr>
            <td> Week 13 (PTM) </td>
            <td> Aug 28 </td>
            <td> Dec 04 </td>
            <td> Mar 19  </td>
        </tr>
        <tr>
            <td rowspan="2"> <b> Holidays </b> </td>
            <td rowspan="2"> None </td>
            <td rowspan="2"> Oct 7 - 9 </td>
            <td> Dec 23 - 2 5 </td>
            
        </tr>
        <tr>
            <td> Dec 30 - Jan 01  </td>
        </tr>
        
    </tbody>
</table>
