+++
date = "2016-03-08"
title = "Young Entrepreneur"
sidemenu = "false"
description = "Entrepreneurship Program"
+++

Every student of Young Entrepreneur will gain an opportunity to participate
and engage in an exciting and unique experience. Like any entreprenuer, they
will journey through the process of taking an idea and executing the plan to
bring the idea to fruition. In the process, they wil gain an understanding towards

* Brainstorming
* Business Planning
* Teamwork
* Product Development
* Market Research
* Business Operations
* Financial Valuation

Through the course, student will engage in the following activities

1. Analyzing business case studies
2. Meeting professionals and entreprenuers
3. Developing a product from scratch
4. Building a busineses plan
5. Pitching the idea to an Angel Investor

Available only during the Weekday Camps.
Student needs to be in Grade 8 or above.