+++
date = "2016-03-08"
title = "Wonder Math"
sidemenu = "false"
description = "Math Program"
+++

Math is all pervasive with application from chess to physics to cooking. 
Wonder Math is an innovative program to help children appreciate problem solving through Math & puzzles. 
Wonder Math builds the foundation required to crack math olympiads. 

We know how to make Math fun! And now here's the chance for your child to improve 
his calculation speed & build his number sense. Our curriculum for the summer 
ensures that your child improves his/her math in a fun-filled way.

The goals of the course are:

* Develop ANS - Approximate Number Sense
* Increase calculation ability & speed
* Breaking down any arithmetic calculations
* lateral thinking through puzzles

We will also be providing a book which contains the math problems and numerous
puzzles according to the student's grade. A summer filled with fun & practice
is the exactly the head start your child needs.
