+++
date = "2016-03-08"
title = "Wonder Words"
sidemenu = "false"
description = "Spelling Bee & Word Program"
+++

If Chess & Rubix can teach a child the importance of problem solving, 
Wonder Words helps a child imbibe value of industry (It’s not called ‘Bee’ for no reason). 

At ZugZwang, we have created an organized and structured approach to preparing
for spelling bee competitions. Furthermore, we’d like to help a child develop 
the love for language & words that go beyond contests & competitive exams.

In this course, we plan to teach a child on how to

* prepare & curate word lists
* engage in word games
* internal spelling bee contests
* learn rules & etiquette of competitions
* online interactive word games
* Special Words & Silent Letters
* Word Origins, Synonyms & Antonyms

We will also be providing a book which contains the word lists and numerous
word games that make preparing for Spell Bee a fun filled activity.

Four of our students from the last summer batch have been successful in 
participating in many spell bee competitions this year at state & national level.
