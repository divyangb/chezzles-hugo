+++
date = "2016-03-08"
title = "Rubix Cubing"
sidemenu = "false"
description = "Rubix Cube Program"
+++

A right of passage for every intellectually inclined student. 
Conquering the spatial complexity created by one of the simplest designs 
by Erno Rubik has had generations fascinated. 

At ZugZwang, we want to help children learn how to break down the complexity
and conquer the problem solving process through Rubix Cube and continue to do 
it under timed conditions. We’d like to help them learn the lesson that 
perfection takes effort, focus & above all patience. 

In this program, a student will learn

* Mathematics of Cubing (permutations & combinations)
* Breaking down the problem
* Introduction to the ZugZwang Method (intrinsic learning, not memorization)
* Introduction to various algorithms (PLL, OLL)
* Habit of performance measurement
* Tips & Tricks to increase speed
* F2L - the last frontier to conquer
* Tournament Rule & Etiquette

Modules

1. 3x3x3 - (ZugZwang Method, OLL & PLL)
2. 2x2x2 - (ZugZwang Method)
3. 3x3x3 - (FLL)
4. 4x4x4 - (Regular & Parities)
5. 5x5x5 - (Regular & Parities)
6. Pyramix

Requirements
Cubes (stickered).
Do not purchase the expensive cubes for the initial levels.
Cubes are available for purchase at ZugZwang.

Available for students from Grade I & above.