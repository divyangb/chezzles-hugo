+++
date = "2016-03-08"
title = "COMP Olympiads"
sidemenu = "false"
description = "Olympiad Preparation"
+++

Most olympiad preparation websites and courses today focus on practice.
One can purchase a plethora of books and question banks and have their
child work on the same. While this approach definitely has an incremental
benefit of solidifying the known concepts, it does not help a child make
the mental leap required to solve higher order problems.

COMP - Critical Thinking, Olympiad Math & Problem Solving is a course
that was created and been functional since 2014. Over the course of 18 months,
it has helped 75% of its students clear level one in SOF Olympiads.

COMP focusses on

* Understanding Problem Solving Methodologies
* Application of these Methodologies in various problems contexts
* Game based learning through Lumosity & PEAK
* Learning through Physical models & Chess (visual learning)
* Interactive Puzzles
* Higher Order Thinking Skills
* Approximate Number Skills
* Puzzles & Approaches

Available for students from Grade IV & above.