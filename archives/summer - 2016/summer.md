+++
date = "2016-03-08"
title = "Summer Program"
sidemenu = "true"
description = "Year 2016"
+++

# ZugZwang Summer Program

ZugZwang is pleased to introduce our new summer camp of 2016 consisting 
of 10 exciting programs! These fun-filled programs are designed to keep 
your child productive and entertained at the same time while school is out! 
If you are interested to enrol for the ZugZwang Summer Camp, please fill out 
the [Registration Form] (../summer/summerenroll/)

For Schedule, please check [Summer Schedule] (../summer/summerschedule/)

For Calendar, please check [Summer Calendar] (../summer/summercalendar/)

Here are the list of programs & descriptions

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th>#</th>
            <th>Program</th>
            <th>Grades</th>
            <th>Type</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td> 1 </td>
            <td> <a href="/program/chess/"> Scholastic Chess </a> </td>
            <td> UKG & Above </td>
            <td> Regular </td>
        </tr>

        <tr>
            <td> 2 </td>
            <td> <a href="/program/rubix/"> Rubix </a> </td>
            <td> Grade 1 & Above </td>
            <td> Regular </td>
        </tr>

        <tr>
            <td> 3 </td>
            <td> <a href="/program/robotix/"> Robotix </a> </td>
            <td> Grade 4 & Above </td>
            <td> Premium </td>
        </tr>
        
        <tr>
            <td> 4 </td>
            <td> <a href="/program/scratch/"> Scratch Coding </a> </td>
            <td> Grade 4 & Above </td>
            <td> Regular </td>
        </tr>
        
        <tr>
            <td> 5 </td>
            <td> <a href="/program/wondermath/"> Wonder Math </a> </td>
            <td> Grade 1 - 8 </td>
            <td> Regular </td>
        </tr>
        
        <tr>
            <td> 6 </td>
            <td> <a href="/program/wonderwords/"> Wonder Words </a> </td>
            <td> Grade 1 - 8 </td>
            <td> Regular </td>
        </tr>
        
        <tr>
            <td> 7 </td>
            <td> <a href="/program/comp/"> COMP Olympiads </a> </td>
            <td> Grade 4 & Above </td>
            <td> Regular </td>
        </tr>
        
        <tr>
            <td> 8 </td>
            <td> <a href="/program/chessleague/"> Chess League </a> </td>
            <td> Rank 02 & Above </td>
            <td> Premium </td>
        </tr>
        
        <tr>
            <td> 9 </td>
            <td> <a href="/program/gameprogramming/"> Game Programming </a> </td>
            <td> Grade 08 & Above </td>
            <td> Regular </td>
        </tr>
        
        <tr>
            <td> 10 </td>
            <td> <a href="/program/youngentrepreneur/"> Young Entrepreneur </a> </td>
            <td> Grade 08 & Above </td>
            <td> Premium </td>
        </tr>
        
    </tbody>
</table>

Regular Course Fee is Rs. 4500 + ST per course

Premium Course Fee is Rs. 5900 + ST per course

For more details click on [Fee Details] (../summer/summerfee/)

We would like to maintain the student to teacher ratio of 10:1. 
So the seats are naturally limited. Please fill in your application as 
early as possible once your summer plans are firmed up.
We also offer transportation on a selective basis.

To Register, please fill out the [Enrollment Form] (https://docs.google.com/a/zugzwang.in/forms/d/1LZMmZgiQ72JemIvMumhvVvy4dPGa4oGarkWMyFTTD5k/viewform#start=embed)