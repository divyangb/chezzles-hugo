+++
date = "2016-03-09"
title = "Summer Calendar"
sidemenu = "false"
description = "Summer Program 2016 Calendar"
+++

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Camp Details </th>
            <th> Start Date </th>
            <th> End Date </th>
            <th> Days </th>
            <th> Timing </th>
            <th> # Sessions </th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td> Camp 1 (APR) </td>
            <td> 05 APR </td>
            <td> 27 APR </td>
            <td> Tue, Wed, Thu & Fri </td>
            <td> 09:00 AM - 06:00 PM </td>
            <td> 14 Sessions </td>
        </tr>
        
        <tr>
            <td> Camp 2 (MAY) </td>
            <td> 05 MAY </td>
            <td> 27 MAY </td>
            <td> Tue, Wed, Thu & Fri </td>
            <td> 09:00 AM - 06:00 PM </td>
            <td> 14 Sessions </td>
        </tr>
        
        <tr>
            <td> Weekend Camp (APR & MAY) </td>
            <td> 09 APR </td>
            <td> 29 MAY </td>
            <td> Sat & Sun </td>
            <td> 09:00 AM - 04:00 PM </td>
            <td> 14 Sessions </td>
        </tr>
    </tbody>
</table>


The default would show the current month. Please click forward button to check out Apr & May.
Please navigate the calendar links below to check out the summer calendar schedule.

<iframe src="https://calendar.google.com/calendar/embed?showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;height=600&amp;wkst=2&amp;bgcolor=%23FFFFFF&amp;src=l6rk8jvk8rd5for7su7k69ph4o%40group.calendar.google.com&amp;color=%2329527A&amp;ctz=Asia%2FCalcutta" style="border:solid 1px #777" width="800" height="600" frameborder="0" scrolling="no"></iframe>