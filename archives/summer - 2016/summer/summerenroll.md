+++
date = "2016-03-09"
title = "Enrollment"
sidemenu = "false"
description = "ZugZwang Summer Program Registration"
+++

### Enrollment Process ###

1. Choose the courses you'd like to enroll.
2. Fill out the Summer Registration Form below
3. Once your admission is approved, you'll receive the invoice
4. Please make the payment through one of our [Payment Methods] (../../payment/)
5. You will receive a registration confirmation mail
6. Ensure you attend the first day of the camp.

<iframe src="https://docs.google.com/forms/d/1LZMmZgiQ72JemIvMumhvVvy4dPGa4oGarkWMyFTTD5k/viewform?embedded=true#start=embed" width="800" height="1600" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>


