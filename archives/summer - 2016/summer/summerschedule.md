+++
date = "2016-03-11"
title = "Summer Schedule"
sidemenu = "false"
description = "Program Schedule"
+++

## Weekday Schedule ##

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Start </th>
            <th> End </th>
            <th> Program 01 </th>
            <th> Program 02 </th>
            <th> Program 03 </th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td> 09:00 </td>
            <td> 10:00 </td>
            <td> <a href="../../program/chess/"> Scholastic Chess </a> </td>
            <td> <a href="../../program/wonderwords/">Wonder Words </a> </td>
            <td> <a href="../../program/scratch/">Scratch Coding </a> </td>
        </tr>

        <tr>
            <td> 10:00 </td>
            <td> 11:00 </td>
            <td> <a href="../../program/comp/"> COMP Olympiads </a> </td>
            <td> Scholastic Chess </td>
            <td> <a href="../../program/rubix/"> Rubix <a/> </td>
        </tr>
        
        <tr>
            <td> 11:00 </td>
            <td> 12:00 </td>
            <td> <a href="../../program/robotix/"> Robotix <a/> </td>
            <td> Scholastic Chess </td>
            <td> <a href="../../program/wondermath/"> Wonder Math <a/> </td>
        </tr>
        
        <tr>
            <td> 12:00 </td>
            <td> 13:00 </td>
            <td> Wonder Words </td>
            <td> Scholastic Chess </td>
            <td> Scratch Coding </td>
        </tr>
        
        <tr>
            <td> 13:00 </td>
            <td> 14:00 </td>
            <td> Scholastic Chess </td>
            <td> COMP Olympiads </td>
            <td> Wonder Math </td>
        </tr>
        
        <tr>
            <td> 14:00 </td>
            <td> 15:00 </td>
            <td> Robotix </td>
            <td> Wonder Math </td>
            <td> Wonder Words </td>
        </tr>
        
        <tr>
            <td> 15:00 </td>
            <td> 16:00 </td>
            <td> Scholastic Chess </td>
            <td> Rubix </td>
            <td> Wonder Math </td>
        </tr>
        
        <tr>
            <td> 16:00 </td>
            <td> 17:00 </td>
            <td> <a href="../../program/gameprogramming/"> Game Programming <a/> </td>
            <td> <a href="../../program/chessleague/"> Chess League <a/> </td>
            <td> Wonder Math </td>
        </tr>
        
        <tr>
            <td> 17:00 </td>
            <td> 18:00 </td>
            <td> <a href="../../program/youngentrepreneur/"> Young Entrepreneur <a/> </td>
            <td> Chess League </td>
            <td> Rubix </td>
        </tr>
        
    </tbody>
</table>

## Weekend Schedule ##

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Start </th>
            <th> End </th>
            <th> Program 01 </th>
            <th> Program 02 </th>
            <th> Program 03 </th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td> 10:00 </td>
            <td> 11:00 </td>
            <td> Wonder Words </td>
            <td> Scholastic Chess </td>
            <td> Wonder Math </td>
        </tr>
        
        <tr>
            <td> 11:00 </td>
            <td> 12:00 </td>
            <td> Game Prog. </td>
            <td> Chess League </td>
            <td> Rubix </td>
        </tr>
        
        <tr>
            <td> 12:00 </td>
            <td> 13:00 </td>
            <td> Wonder Math </td>
            <td> Chess League </td>
            <td> Rubix </td>
        </tr>
        
        <tr>
            <td> 13:00 </td>
            <td> 14:00 </td>
            <td> Scratch Coding </td>
            <td> Rubix </td>
            <td> Wonder Math </td>
        </tr>
        
        <tr>
            <td> 14:00 </td>
            <td> 15:00 </td>
            <td> Wonder Words </td>
            <td> Robotix </td>
            <td> COMP </td>
        </tr>
        
        <tr>
            <td> 15:00 </td>
            <td> 16:00 </td>
            <td> Scholastic Chess </td>
            <td> Wonder Words </td>
            <td> Wonder Math </td>
        </tr>

    </tbody>
</table>