+++
date = "2016-03-09"
title = "Fee Details"
sidemenu = "Fase"
description = "Summer Program 2016"
+++

### Course Fee Details ###

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Course Type </th>
            <th> Course Fee </th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td> Regular </td>
            <td> Rs. 4500 + ST </td>
        </tr>
        
        <tr>
            <td> Premium </td>
            <td> Rs. 5900 + ST </td>
        </tr>
    </tbody>
</table>

### Regular Courses ###

<table class="pure-table pure-table-striped pure-table-horizontal">
    <tbody>
        <tr>
            <td> Scholastic Chess </td>
            <td> Rubix Cube </td>
            <td> Scratch Coding </td>
            <td> Wonder Math </td>
        </tr>
        
        <tr>
            <td> Wonder Words </td>
            <td> COMP Olympiads </td>
            <td> Chess League </td>
            <td> Game Programming </td>
        </tr>
    </tbody>
</table>

### Premium Courses ###

<table class="pure-table pure-table-striped pure-table-horizontal">
    <tbody>
        <tr>
            <td> Robotix </td>
            <td> Young Entrepreneur </td>
        </tr>
    </tbody>
</table>

### Discounts ###

If you enroll in more than 1 course, you can avail the following discounts

* 1 course - no discount
* 2 courses - 10% off the 2nd course
* 3 courses or more - 25% off the 3rd course & above

No discounts are available for premium courses.
There are no sibling discounts.

Here is a sample worked out to determine the fee due. 

<table class="pure-table pure-table-striped pure-table-horizontal">
    <thead>
        <tr>
            <th> Combo </th>
            <th> Courses Enrolled </th>
            <th> Course Type </th>
            <th> Discount </th>
            <th> Total Fee </th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td> Combo One </td>
            <td> Chess, Rubix & Wonder Math </td>
            <td> 3 Regular Courses </td>
            <td> (4500 + 4050 + 3375) </td>
            <td> 11925 + ST </td>
        </tr>
        
        <tr>
            <td> Combo Two </td>
            <td> Chess, Rubix & Robotix </td>
            <td> 2 Regular + 1 Premium </td>
            <td> (5900 + 4050 + 3375) </td>
            <td> 13325 + ST </td>
        </tr>
        
        <tr>
            <td> Combo Three </td>
            <td> Wonder Words & Scratch Coding </td>
            <td> 2 Regular </td>
            <td> (4500 + 4050) </td>
            <td> 9550 + ST </td>
        </tr>
        
        <tr>
            <td> Combo Four </td>
            <td> Game Programming & Young Entrepreneur </td>
            <td> 1 Regular + 1 Premium </td>
            <td> (4050 + 5900) </td>
            <td> 9950 + ST </td>
        </tr>
    </tbody>
</table>

Please note that the above calculation does not include Admission Fee.

If you plan to enroll for both camps (APR & MAY), you can avail a further
20% discount on the second camp fee. Different modules will be covered in both
camps and will not be a repetition.